# dios_mjpeg

## Description
tiny actix-and-tokio-and-ffmpeg server that pulls a streamm from soom.... where and serves it as mpjpeg over http. created for dios/cohost crossposting end embeds because we live in hell

## Installation
no installation only serve dog meme

## Usage
`dios_mjpeg` is configured ~entirely via environment variables ( and in theory `.env` support although i'm not sure that ... works rn. )

### At startup

variables you might want to set:
- `HOST=localhost` host for bind address
- `PORT=8080` port for bind address
- `HTTPS=` whether or not to use secure cookies ( fetched, but ignored since there's not any cookies rn )
- `MJPEG_COMMAND=ffmpeg` command to be run in the command runner. currently in practice this should be `scripts/command.sh` or something
- `MJPEG_STREAM_LOCATION=/mjpeg_stream` route to serve mpjpeg stream on
- `MJPEG_CONTROL_LOCATION=/mjpeg_control` route for receiving control

if you are using `scripts/command.sh` as `MJPEG_COMMAND`, you will also want to set
- `MJPEG_SOURCE=` ( where ffmpeg should fetch the stream to turn into mpjpeg from e.g., `http://my.streaming.server/hls/stream/index.m3u8` )
- `MJPEG_FRAMERATE=`: framerate for the stream ( e.g., `5` )
- `MJPEG_SCALE=` : value passed to `-vf scale=<...>` to set stream size ( e.g., `800:-1` )

### At runtime

dios_mjpeg is controlled by `POST`ing json to `${MJPEG_CONTROL_LOCATION}`. currently the only allowed json bodies are of the form
- `{ "state" : <state> }`

where state is one of:
- `"play"`
- `"pause"`
- `"stop"`

you can also `GET` `${MJPEG_CONTROL_LOCATION}` and it'll return the same information as what was posted ( in theory, unless it's startup in which case it'll be `stop`)

#### `play`
run `${MJPEG_COMMAND}` and make whatever it prints to `stdout` available via `GET` from `${MJPEG_STREAM_LOCATION}`

as long as dios_mjpeg is in `play` state the command will be restarted if it exits for any reason. currently there's a maximum restart rate of once per second that's hardcoded in

NOTE: this means that if you tell it to play and `${MJPEG_SOURCE}` doesn't exist - for example because the stream it's supposed to be pulling isn't available yet - the command will probably die a painful but rapid death every second until you tell it to stop or the stream comes online. this also means that if the command is simply wrong, it will die in a loop forever.

#### `pause`
abruptly stop the command if any that's running. no more video will be available until the next `play`. however, any connections which are already open will be left open, and any new connections that come in will be hooked up waiting for data. if they stay open and don't time out they will resume / begin playing when the next `play` happens

a `pause` from stopped state will allow new connections, although they won't get any data until the next `play`

#### `stop`
abruptly stop the command if any that's running, like in `pause`. also let the `mpjpeg` feed end. anyone who was watching, and anyone who starts watching while stopped will *not* start receiving a stream.

## License
probably gonna end up anti-capitalist software license ( v 1.4 ) or compatible but honestly i don't know if this project will even exist as freestanding for more than like a day
