use actix::prelude::*;
use tokio::sync::{oneshot, broadcast};
use tokio::io::{AsyncReadExt,AsyncBufReadExt};
use tokio::process::Command;
use std::process::Stdio;



#[derive(Message)]
#[rtype(result = "State")]
pub struct MjpegQuery {}

#[derive(Serialize, Deserialize)]
pub enum State {
#[serde(rename = "play")]
    Play,
#[serde(rename = "pause")]
    Pause,
#[serde(rename = "stop")]
    Stop,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct MjpegPause { }

#[derive(Message)]
#[rtype(result = "()")]
pub struct MjpegStop { }

#[derive(Message)]
#[rtype(result = "()")]
pub struct MjpegPlay { }

#[derive(Message)]
#[rtype(result = "Option<broadcast::Receiver<actix_web::web::Bytes> >")]
pub struct MjpegSubscribe { }




pub struct Mjpeg {
    pub command: String,
    pub capacity: usize,
    pub data_sender: Option<broadcast::Sender<actix_web::web::Bytes>>,
    pub pause_sender: Option<oneshot::Sender<()>>,
}

impl Mjpeg {
    pub fn new(command: String, capacity: usize) -> Mjpeg {
        let ( tx, mut _rx ) = broadcast::channel(capacity);
        Mjpeg {
            command: command,
            capacity: capacity,
            data_sender: Some(tx),
            pause_sender: None,
        }
    }
}

impl Actor for Mjpeg {
    type Context = Context<Self>;
}

impl Handler<MjpegPause> for Mjpeg {
    type Result = ();
    fn handle(&mut self, _message: MjpegPause, _context: &mut Context<Self>) -> Self::Result {
        if let Some(tx) = self.pause_sender.take() {
            let _ = tx.send(());
        }
        if self.data_sender.is_none() {
            let ( tx, mut _rx ) = broadcast::channel::<actix_web::web::Bytes>(self.capacity);
            self.data_sender.replace(tx.clone());
        }
    }
}

impl Handler<MjpegStop> for Mjpeg {
    type Result = ();
    fn handle(&mut self, _message: MjpegStop, _context: &mut Context<Self>) -> Self::Result {
        if let Some(tx) = self.pause_sender.take() {
            let _ = tx.send(());
        }
        self.data_sender = None;
    }
}

impl Handler<MjpegSubscribe> for Mjpeg {
    type Result = Option<broadcast::Receiver<actix_web::web::Bytes> >;
    fn handle(&mut self, _message: MjpegSubscribe, _context: &mut Context<Self>) -> Self::Result {
        match &mut self.data_sender {
            Some(tx) => Some(tx.subscribe()),
            _ => None
        }
    }
}

impl Handler<MjpegQuery> for Mjpeg {
    type Result = MessageResult<MjpegQuery>;
    fn handle(&mut self, _message: MjpegQuery, _context: &mut Context<Self>) -> Self::Result {
        let state = if self.pause_sender.is_some() {
            State::Play
        } else if self.data_sender.is_some() {
            State::Pause
        } else {
            State::Stop
        };
        MessageResult(state)
    }
}

impl Handler<MjpegPlay> for Mjpeg {
    type Result = ();
    fn handle(&mut self, _message: MjpegPlay, _context: &mut Context<Self>) -> Self::Result {
        if self.pause_sender.is_some() {
            return ();
        }
        let ( pause_tx, mut pause_rx) = oneshot::channel::<()>();
        self.pause_sender.replace(pause_tx);

        let data_tx = {
            match &mut self.data_sender {
                Some(tx) => tx.clone(),
                None => {
                    let ( tx, mut _rx ) = broadcast::channel::<actix_web::web::Bytes>(self.capacity);
                    self.data_sender.replace(tx.clone());
                    tx
                }
            }
        };
        let command = self.command.clone();
        tokio::spawn(async move {
            'outer: loop {
                let mut buf : [u8; 8192] = [0; 8192];
                let sleep = tokio::time::sleep(tokio::time::Duration::from_millis(1000));
                let mut child = Command::new(&command)
                    .stdout(Stdio::piped())
                    .spawn()
                    .expect("Failed to launch command!");
                let mut stdout = child.stdout.take().unwrap();
                let ( kill_tx, kill_rx ) = oneshot::channel::<()>();
                tokio::spawn(async move {
                    tokio::select! {
                        _ = child.wait() => {}
                        _ = kill_rx => child.kill().await.expect("Failed to kill child")
                    }
                });

                loop {
                    let count : usize ;
                    tokio::select! {
                        _ = &mut pause_rx => {
                            let _ = kill_tx.send(());
                            break 'outer;
                        }
                        maybe_count = stdout.read(&mut buf[..]) => {
                            match maybe_count {
                                Ok(got_count) if got_count > 0 => {
                                    count = got_count.clone();
                                }
                                _ => {
                                    let _ = kill_tx.send(());
                                    break;
                                }
                            }
                        }
                    }
                    let _ = data_tx.send(actix_web::web::Bytes::copy_from_slice(&buf[0..count]));
                }

                tokio::select! {
                    _ = &mut pause_rx => { break; }
                    _ = sleep => { }
                }
            }
        });
    }
}

impl Drop for Mjpeg {
    fn drop(&mut self) {
        if let Some(tx) = self.pause_sender.take() {
            let _ = tx.send(());
        }
    }
}

