#[macro_use] extern crate log;
#[macro_use] extern crate serde;
extern crate dotenv;
use actix::*;
//use actix_identity::*;
//use actix_session::{SessionMiddleware, storage::CookieSessionStore};
use actix_web::{web, middleware, App, HttpServer,HttpRequest};
use actix_web::{post,get, HttpResponse};
use actix_http::header;
use dotenv::dotenv;
use env_logger;
use listenfd::ListenFd;
use std::env;
use std::path::Path;
pub mod mjpeg_context;
use mjpeg_context::{Mjpeg,MjpegSubscribe, MjpegPause, MjpegPlay, MjpegStop, MjpegQuery, State};
use serde::Deserialize;
use serde::Serialize;

#[derive(Serialize, Deserialize)]
pub struct MjpegControl {
    state: State,
}

pub async fn mjpeg_stream(
    _req: HttpRequest,
    mjpeg_addr: web::Data<actix::Addr<Mjpeg> >,
) -> HttpResponse {
    if let Some(rx) = mjpeg_addr.send(MjpegSubscribe {}).await.unwrap() {
        return HttpResponse::Ok()
            .insert_header((header::CONTENT_TYPE, "multipart/x-mixed-replace;boundary=--ffmpeg"))
            .insert_header((header::CACHE_CONTROL, "no-store"))
            .insert_header((header::CONNECTION, "close"))
            .streaming(tokio_stream::wrappers::BroadcastStream::new(rx))
    } else {
        return HttpResponse::NotFound()
            .finish()
    }
}

pub async fn mjpeg_control(
    mjpeg_addr: web::Data<actix::Addr<Mjpeg>>,
    mjpeg_control : web::Json<MjpegControl>,
) -> HttpResponse {

    match mjpeg_control.state {
        State::Play => { let _ = mjpeg_addr.send(MjpegPlay {}).await.unwrap();}
        State::Pause => { let _ = mjpeg_addr.send(MjpegPause {}).await.unwrap();}
        State::Stop => { let _ = mjpeg_addr.send(MjpegStop{}).await.unwrap();}
        _ => { return HttpResponse::NotImplemented().finish();}
    }
    return HttpResponse::Ok()
        .finish()
}
pub async fn mjpeg_query(
    mjpeg_addr: web::Data<actix::Addr<Mjpeg>>,
) -> HttpResponse {

    let res = mjpeg_addr.send(MjpegQuery {}).await.unwrap();
    return HttpResponse::Ok()
        .json(&MjpegControl { state: res })
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info")
    ).init();

    // HOST and PORT set the bind address
    let host_env = "HOST";
    let host = match env::var(host_env) {
        Ok(val) => val,
        Err(_) => String::from("localhost"),
    };
    let port_env = "PORT";
    let port = match env::var(port_env) {
        Ok(val) => val,
        Err(_) => String::from("8080"),
    };
    eprintln!("PORT: {}",port);
    eprintln!("HOST: {}",host);
    let bind_address = format!("{}:{}", host, port);


    // for whether or not to use secure cookies
    let is_https = match env::var("HTTPS") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    let mjpeg_command_env = "MJPEG_COMMAND";
    let mjpeg_command = match env::var(mjpeg_command_env) {
        Ok(val) => val,
        Err(_) => String::from("ffmpeg"),
    };

    let mjpeg_stream_location_env = "MJPEG_STREAM_LOCATION";
    let mjpeg_stream_location = match env::var(mjpeg_stream_location_env) {
        Ok(val) => val,
        Err(_) => String::from("/mjpeg_stream"),
    };
    let mjpeg_control_location_env = "MJPEG_CONTROL_LOCATION";
    let mjpeg_control_location = match env::var(mjpeg_control_location_env) {
        Ok(val) => val,
        Err(_) => String::from("/mjpeg_control"),
    };

    let mjpeg_server = mjpeg_context::Mjpeg::new(mjpeg_command.clone(),16).start();
    let mut server = HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(mjpeg_server.clone()))
/*            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&env::var("AUTH_KEY").expect("AUTH_KEY must be set").as_bytes())
                    .name("auth-cookie")
                    .secure(is_https)
            ))
            .wrap(SessionMiddleware::new(CookieSessionStore::default(), Key::from(&env::var("SESSION_KEY").expect("SESSION_KEY must be set").as_bytes())))*/
            .wrap(middleware::Logger::default())
            .route(&mjpeg_stream_location,web::get().to(mjpeg_stream))
            .route(&mjpeg_control_location,web::post().to(mjpeg_control))
            .route(&mjpeg_control_location,web::get().to(mjpeg_query))
    });

    // https://actix.rs/docs/autoreload/
    let mut listenfd = ListenFd::from_env();
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind(bind_address)?
    };

    let res = server.run().await;

    res
}
